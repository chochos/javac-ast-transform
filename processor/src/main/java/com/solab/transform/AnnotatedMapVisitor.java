package com.solab.transform;

import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a javac visitor that will analyze a class, going into all its methods
 * looking for "get" calls on the annotated map specified for the visitor.
 * It will gather all the keys and then add an initializer to the field,
 * with the prefix set on the annotation and a list of all the keys found on the code.
 * This allows the map to be populated upon creation, so that it never
 * returns null for a key.
 *
 * @author Enrique Zamudio
 * Date: 10/8/18 7:58 PM
 */
public class AnnotatedMapVisitor extends JCTree.Visitor {

    private final JCTree.JCVariableDecl map;
    private final Context context;
    private final List<String> statements = new ArrayList<>();
    private final String prefix;

    public AnnotatedMapVisitor(String prefix, JCTree.JCVariableDecl ini, Context context) {
        map = ini;
        this.context = context;
        this.prefix = prefix;
    }

    public void transform(TreeMaker maker) {
        try {
            //Create the ID for the MapHelper class
            Names names = Names.instance(context);
            JCTree.JCExpression subject = maker.Type(JavacElements.instance(context).getTypeElement("com.solab.helper.MapHelper").type);
            //Now the ID for the static method we'll call on it
            Name method = names.fromString("create");
            //These are the arguments for the call
            JCTree.JCExpression[] exprargs = new JCTree.JCExpression[statements.size() + 1];
            //The first one is the prefix
            exprargs[0] = maker.Literal(prefix);
            //And then the rest are the keys we found
            for (int i = 1; i <exprargs.length; i++) {
                exprargs[i] = maker.Literal(statements.get(i-1));
            }
            //Create the invocation
            JCTree.JCExpression call = maker.Select(subject, method);
            call.setType(map.vartype.type);
            //Add it as the field initializer
            map.init = maker.App(call, com.sun.tools.javac.util.List.from(exprargs));
        } catch (Exception ex) {
            //TODO to something more graceful...
            System.out.println("Something bad happened");
            ex.printStackTrace(System.out);
        }
    }

    @Override
    public void visitExec(JCTree.JCExpressionStatement st) {
        st.getExpression().accept(this);
    }

    @Override
    public void visitApply(JCTree.JCMethodInvocation inv) {
        //We're only interested in method calls on fields
        if (inv.getMethodSelect() instanceof JCTree.JCFieldAccess) {
            JCTree.JCFieldAccess fa = ((JCTree.JCFieldAccess)inv.getMethodSelect());
            //Specifically, calls on "get" methods on our annotated field
            if (fa.getExpression() instanceof JCTree.JCIdent &&
                    map.getName().equals(((JCTree.JCIdent)fa.getExpression()).getName()) &&
                    fa.getIdentifier().toString().equals("get")) {
                //If the first argument is a literal, collect it for later
                JCTree.JCExpression arg = inv.getArguments().get(0);
                if (arg instanceof JCTree.JCLiteral) {
                    statements.add(((JCTree.JCLiteral)arg).getValue().toString());
                }
            }
        }
        //Go into the arguments so we can find nested calls
        for (JCTree.JCExpression t : inv.getArguments()) {
            t.accept(this);
        }
    }

    @Override
    public void visitClassDef(JCTree.JCClassDecl jcClassDecl) {
        //Visit the methods of a class
        for (JCTree m : jcClassDecl.getMembers()) {
            if (m instanceof JCTree.JCMethodDecl) {
                m.accept(this);
            }
        }
    }

    @Override
    public void visitMethodDef(JCTree.JCMethodDecl jcMethodDecl) {
        //Visit the statements of a method's body
        for (JCTree.JCStatement st : jcMethodDecl.getBody().getStatements()) {
            st.accept(this);
        }
    }

    @Override
    public void visitTree(JCTree jcTree) {
        //Deadend, terminator
        //If we don't override this with an empty impl, we'll get an exception
    }
}
