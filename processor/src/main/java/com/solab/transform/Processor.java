package com.solab.transform;

import com.solab.helper.MapLoader;
import com.sun.source.util.Trees;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import java.util.Collections;
import java.util.Set;

/** This annotation processor is invoked by the compiler.
 * When a @MapLoader annotation is found, this gets called.
 * It will get the annotation's prefix, and then create a visitor
 * that looks for "get" calls on the annotated map, with a string
 * literal as argument, to gather all the keys that will be looked
 * for.
 *
 * @author chochos
 */
@SupportedAnnotationTypes("com.solab.helper.MapLoader")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class Processor implements javax.annotation.processing.Processor {

    private JavacProcessingEnvironment env;
    private Trees trees;
    private TreeMaker maker;
    private Context context;

    @Override
    public Set<String> getSupportedOptions() {
        return Collections.emptySet();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton("com.solab.helper.MapLoader");
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_8;
    }

    @Override
    public void init(ProcessingEnvironment processingEnv) {
        env = (JavacProcessingEnvironment)processingEnv;
        trees = Trees.instance(processingEnv);
        maker = TreeMaker.instance(env.getContext());
        context = env.getContext();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        //Go through the specified annotations
        for (TypeElement ann : annotations) {
            Set<? extends Element> elems = roundEnv.getElementsAnnotatedWith(ann);
            for (Element e : elems) {
                //Make sure the annotated field is a map
                if (!e.asType().toString().equals("java.util.Map<java.lang.String,java.lang.String>")) {
                    System.out.println("TODO!!!! error, you must only annotate fields of type Map<String,String>");
                }
                //Get the prefix specified in the annotation
                MapLoader a = e.getAnnotation(MapLoader.class);
                //Get the enclosing type (we know it's a type because
                //the annotation can only be used on fields)
                JCTree tree = (JCTree)trees.getTree(e.getEnclosingElement());
                //Create the visitor and run it on the type
                AnnotatedMapVisitor omv = new AnnotatedMapVisitor(a.value(),
                        (JCTree.JCVariableDecl)trees.getTree(e), context);
                tree.accept(omv);
                //Analysis done, now do the transformation
                omv.transform(maker);
            }
        }
        return false;
    }

    @Override
    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation,
                                                         ExecutableElement member, String userText) {
        return null;
    }

}
