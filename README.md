# javac-ast-transform

This is an example of a simple AST transformation using the
annotation processor API to find the starting point, then
using the javac API to analyze a class AST and adding an
initializer to the annotated field.

It's split into three modules:

* **library** is a simple library which contains the annotation
  to use on a `Map`, as well as a helper class that will do the
  initialization.
* **processor** is a compile-time only module which contains
  the annotation processor, which will be called when the compiler
  finds an annotated element, and a javac visitor that will
  add the initializer to the field.
* **example** contains a simple class with an annotated field
  to demonstrate how it's transformed at compile time.

## Building

Just type `./gradlew build`. If you want to open this in IntelliJ,
type `./gradlew idea` and open the generated `ipr` file.

To run the file, type `./gradlew example:run`, or you can run it directly:

`java -cp library/build/libs/library-1.0.jar:example/build/libs/example-1.0.jar com.solab.example.Example`

This way you can define some properties in the command line, such
as `-Dexample.algo=FOO -Dexample.otro=BAR` etc.

I wrote and tested this with Java 8. I still need to test with
Java 11, as I really have no idea if it will work or not.
