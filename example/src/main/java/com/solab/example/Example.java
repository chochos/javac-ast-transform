package com.solab.example;

import com.solab.helper.MapLoader;

import java.util.Map;

/** Simple example of how AST transformations work.
 * This class has an annotated field which will be
 * transformed at compile time (an initializer will be added).
 *
 * @author chochos
 */
public class Example {

    /** This map will be initialized and populated with
     * code added at compile time.
     * It will be populated with keys having the prefix
     * specified in the annotation, and whatever is called
     * on the code below.
     */
    @MapLoader("example")
    private Map<String, String> map;

    public void foo() {
        //This will look for "example.algo"
        System.out.println(map.get("algo"));
    }

    protected void bar() {
        System.out.println(map.get("otro"));
    }
    private void baz() {
        System.out.println(map.get("mas"));
    }

    public static void main(String... args) {
        Example e = new Example();
        e.foo();
        e.bar();
        e.baz();
        System.out.println(e.map);
    }
}
