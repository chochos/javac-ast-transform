package com.solab.helper;

import java.util.Map;
import java.util.HashMap;

public class MapHelper {

  public static Map<String,String> create(String prefix, String... keys) {
    HashMap<String, String> m = new HashMap<>(keys.length);
    for (String k : keys) {
      String prop = System.getProperty(prefix + "." + k);
      m.put(k, prop == null ? "MISSING PROPERTY!" : prop);
    }
    return m;
  }

}

