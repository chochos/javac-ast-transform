package com.solab.helper;

import java.lang.annotation.Target;
import java.lang.annotation.Retention;

@Target(java.lang.annotation.ElementType.FIELD)
@Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public @interface MapLoader {
  String value();
}
